import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MealsController } from './meals/meals.controller';
import { FoodsController } from './foods/foods.controller';
import { AuthController } from './auth/auth.controller';
import { FoodsService } from './foods/foods.service';
import { FoodsModule } from './foods/foods.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { MealsService } from './meals/meals.service';
import { MealsModule } from './meals/meals.module';
import { AuthModule } from './auth/auth.module';
@Module({
  imports: [FoodsModule,TypeOrmModule.forRoot(typeOrmConfig), MealsModule, AuthModule],
  controllers: [AppController, MealsController, FoodsController, AuthController],
  providers: [AppService],
})
export class AppModule {}
