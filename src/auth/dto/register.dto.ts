import { IsString, MinLength, MaxLength, Matches, IsNumber, IsBoolean } from 'class-validator';

export class RegisterDto {
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  username: string;

  @IsString()
  @MinLength(8)
  @MaxLength(20)
  @Matches(
    /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
    { message: 'password too weak' },
  )
  @IsString()
  password: string;
  @IsNumber()
  height : number;
  @IsNumber()
  weight : number;
  @IsBoolean()
  gender:boolean;
  @IsNumber()
  points:number;
  @IsNumber()
  age:number;
}
