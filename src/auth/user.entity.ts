import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, Unique, OneToMany } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Meal } from 'src/meals/meal.entity';
@Entity()
@Unique(['username'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column()
  salt: string;

  @OneToMany(type => Meal, meal => meal.user, { eager: false })
  meals: Meal[];
  @Column()
  height : number;
  @Column()
  weight:number;
  @Column()
  gender:boolean;
  @Column()
  points:number;
  @Column()
  age:number;
  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }
}
