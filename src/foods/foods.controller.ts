import { Controller, Get, Query, ValidationPipe, Post, UsePipes, Body, Delete, Param, ParseIntPipe } from '@nestjs/common';
import { FoodsService } from './foods.service';
import { GetFoodsFilterDto } from './dto/get-foods-filter.dto';
import { Food } from './food.entity';
import { CreateFoodDto } from './dto/create-food.dto';

@Controller('foods')
export class FoodsController {
    constructor(private foodService : FoodsService){}
    @Get()
    getFoods(@Query(ValidationPipe) filterDto : GetFoodsFilterDto) {
        return this.foodService.getFoods(filterDto);
    }
    
    @Post()
    @UsePipes(ValidationPipe)
    createFood(@Body() createFoodDto: CreateFoodDto){
        return this.foodService.createFood(createFoodDto);
    }
    @Delete('/:id')
    deleteTask(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.foodService.deleteTask(id);
  }
}
