import { Injectable, NotFoundException } from '@nestjs/common';
import { FoodRepository } from './foods.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { GetFoodsFilterDto } from './dto/get-foods-filter.dto';
import { Food } from './food.entity';
import { CreateFoodDto } from './dto/create-food.dto';
@Injectable()
export class FoodsService {
  
    constructor(@InjectRepository(FoodRepository) private foodRepository : FoodRepository){

    }
    async getFoods(filterFoodDto : GetFoodsFilterDto) : Promise<Food[]>
    {
        return this.foodRepository.getFoods(filterFoodDto);
    }
    async createFood(
        createFoodDto: CreateFoodDto
      ): Promise<Food> {
        return this.foodRepository.createFood(createFoodDto);
      }
      async deleteTask(
        id: number
      ): Promise<void> {
        const result = await this.foodRepository.delete({id});
    
        if (result.affected === 0) {
          throw new NotFoundException(`Food with ID "${id}" not found`);
        }
      }
       
}
