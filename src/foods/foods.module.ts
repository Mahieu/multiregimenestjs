import { Module } from '@nestjs/common';
import { FoodRepository } from './foods.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FoodsController } from './foods.controller';
import { FoodsService } from './foods.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([FoodRepository])
    ],
      controllers: [FoodsController],
      providers: [FoodsService],
      exports : [FoodsService]
})
export class FoodsModule {}
