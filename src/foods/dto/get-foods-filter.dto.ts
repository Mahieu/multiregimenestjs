import { IsNotEmpty, IsOptional } from "class-validator";


export class GetFoodsFilterDto{
    @IsOptional()
    @IsNotEmpty()
    search:string;
    @IsOptional()
    @IsNotEmpty()
    limit: string;
    @IsOptional()
    @IsNotEmpty()
    offset: string;
}