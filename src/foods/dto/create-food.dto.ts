import { IsNotEmpty } from 'class-validator';
export class CreateFoodDto {
    @IsNotEmpty()
    name:string;
    @IsNotEmpty()
    displayName: string;
    @IsNotEmpty()
    portionDisplayName: string;
    @IsNotEmpty()
    servingDesc: string;
    @IsNotEmpty()
    category : string;
    @IsNotEmpty()
    points : number;
    @IsNotEmpty()
    type : string;
    @IsNotEmpty()
    weight : string;
    @IsNotEmpty()
    weightType : string;
    @IsNotEmpty()
    brand : string;
}