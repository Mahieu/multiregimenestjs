import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
@Entity()
export class Food extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    name:string;
    @Column({name : "displayname"})
    displayName: string;
    @Column({name : "portiondisplayname"})
    portionDisplayName: string;
    @Column({name : "servingdesc"})
    servingDesc: string;
    @Column()
    category : string;
    @Column()
    points : number;
    @Column()
    type : string;
    @Column()
    weight : string;
    @Column({name : "weighttype"})
    weightType : string;
    @Column()
    brand : string;
}