import { Repository, EntityRepository } from "typeorm";
import { Food } from "./food.entity";
import { GetFoodsFilterDto } from "./dto/get-foods-filter.dto";
import { CreateFoodDto } from "./dto/create-food.dto";

@EntityRepository(Food)
export class FoodRepository extends Repository<Food> {
    async getFoods(filterDto:GetFoodsFilterDto) : Promise<Food[]>{
        const {search,limit,offset} = filterDto;
        console.log(filterDto);
        const query = this.createQueryBuilder('food');
        if (search) {
            query.andWhere('(food.displayName LIKE :search OR food.category LIKE :search)', { search: `%${search}%` });
          }
        if(limit){
            query.limit(Number(limit));
        }
        if(offset){
            query.offset(Number(offset));
        }
        const foods = await query.getMany();
        return foods;
    }
    async createFood(createFoodDto : CreateFoodDto) : Promise<Food>
    {
        const { name,
            displayName,
            portionDisplayName,
            servingDesc,
            category ,
            points ,
            type ,
            weight ,
            weightType ,
            brand} = createFoodDto;
            const food = new Food();
            food.name = name;
            food.displayName = displayName;
            food.servingDesc = servingDesc;
            food.portionDisplayName = portionDisplayName;
            food.category = category;
            food.points = points;
            food.type = type;
            food.weight=weight;
            food.weightType = weightType;
            food.brand = brand;
            await food.save();
            return food;
    }
}