import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MealRepository } from './meals.repository';
import { GetMealsFilterDto } from './dto/get-meals.dto';
import { Meal } from './meal.entity';
import { CreateMealDto } from './dto/create-meal.dto';
import { User } from 'src/auth/user.entity';

@Injectable()
export class MealsService {

    constructor(@InjectRepository(MealRepository) private mealRepository : MealRepository){

    }
    async getMeals(filterMealDto : GetMealsFilterDto,user:User) : Promise<Meal[]>
    {
        return this.mealRepository.getMeals(filterMealDto,user);
    }
    async createMeal(
        createMealDto: CreateMealDto,user : User
      ): Promise<Meal> {
        return this.mealRepository.createMeal(createMealDto,user);
      }
      /** 
    async deleteMeal(
        id: number
      ): Promise<void> {
        const result = await this.deleteMeal.delete({id});
    
        if (result.affected === 0) {
          throw new NotFoundException(`Food with ID "${id}" not found`);
        }
      }
    */
}
