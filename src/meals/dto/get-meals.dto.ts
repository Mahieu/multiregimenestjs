import { IsNotEmpty, IsOptional } from "class-validator";


export class GetMealsFilterDto{
    @IsOptional()
    @IsNotEmpty()
    startDate:string;
    @IsOptional()
    @IsNotEmpty()
    endDate:string;
}