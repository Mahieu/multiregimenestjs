import { IsNotEmpty } from "class-validator";
import { Food } from "src/foods/food.entity";

export class CreateMealDto {
    @IsNotEmpty()
    quantity : number;
    @IsNotEmpty()
    mealTime : Date;
    @IsNotEmpty()
    food : Food;
}