import { Controller, Get, ValidationPipe, Query, Post, UsePipes, Body, Delete, Param, ParseIntPipe, UseGuards } from '@nestjs/common';
import { MealsService } from './meals.service';
import { GetMealsFilterDto } from './dto/get-meals.dto';
import { CreateMealDto } from './dto/create-meal.dto';
import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/auth/user.entity';
import { AuthGuard } from '@nestjs/passport';

@Controller('meals')
@UseGuards(AuthGuard())
export class MealsController {
    constructor(private mealService : MealsService){}
    @Get()
    getFoods(@Query(ValidationPipe) filterDto : GetMealsFilterDto,@GetUser() user: User) {
        return this.mealService.getMeals(filterDto,user);
    }
    
    @Post()
    @UsePipes(ValidationPipe)
    createFood(@Body() createMealDto: CreateMealDto,@GetUser() user: User){
        return this.mealService.createMeal(createMealDto,user);
    }
    @Delete('/:id')
    deleteTask(@Param('id', ParseIntPipe) id: number) {
    //return this.foodService.deleteTask(id);
  }
}
