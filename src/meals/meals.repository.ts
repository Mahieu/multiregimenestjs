import { Repository, EntityRepository, MoreThanOrEqual, LessThanOrEqual, Between } from "typeorm";
import { Meal } from "./meal.entity";
import { GetMealsFilterDto } from "./dto/get-meals.dto";
import { CreateMealDto } from "./dto/create-meal.dto";
import { User } from "src/auth/user.entity";
@EntityRepository(Meal)
export class MealRepository extends Repository<Meal> {
    async getMeals(filterDto : GetMealsFilterDto,user : User): Promise<Meal[]>
    {
        const {startDate,endDate} = filterDto;
        let findParameters = {};
        if(startDate && endDate)
        {
          findParameters = {mealTime : Between(startDate,endDate),where : {userId: user.id}};
        }
        else if (startDate) {
            findParameters = {mealTime : MoreThanOrEqual(startDate)};
          }
        else if (endDate) {
            findParameters = {mealTime : LessThanOrEqual(endDate)};
          }
        const meals = await this.find(findParameters);
        return meals;
    }
    async createMeal(createDto : CreateMealDto,user : User): Promise<Meal>
    {
        const {food,mealTime,quantity} = createDto;
        const meal = new Meal();
        meal.food = food;
        meal.mealTime = new Date(mealTime);
        meal.quantity = quantity;
        meal.user = user;
        meal.userId = user.id;
        await meal.save();
        delete meal.user;
        return meal;
    }
}
