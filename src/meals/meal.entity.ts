import { Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, BaseEntity, ManyToOne } from "typeorm";
import { Food } from "src/foods/food.entity";
import { User } from "src/auth/user.entity";

@Entity()
export class Meal extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    quantity: number;
    @Column({name: "mealtime"})
    mealTime : Date;
    @ManyToOne(type => Food,{ eager: true })
    food: Food;
    @ManyToOne(type => User, user => user.meals, { eager: false })
    user: User;
  
    @Column()
    userId: number;
  }
