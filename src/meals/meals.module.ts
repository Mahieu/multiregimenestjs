import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MealRepository } from './meals.repository';
import { MealsController } from './meals.controller';
import { MealsService } from './meals.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([MealRepository])
    ],
      controllers: [MealsController],
      providers: [MealsService],
      exports : [MealsService]
})
export class MealsModule {
    
}
